This project was bootstrapped with [Create React App](https://github.com/facebook/create-react-app).

# Activity Tracker

We gather the data from the users and store them.

Provides the user Interface to the user who wants to monitor the activity by providing the calendar to select a particular date to see the activity.

<br/>
<br/>
<br/>

## Available Scripts

In the project directory, you can run:

### `npm start`

Runs the app in the development mode.<br />
Open [http://localhost:3000](http://localhost:3000) to view it in the browser.

The page will reload if you make edits.<br />
You will also see any lint errors in the console.

### `npm test`

Launches the test runner in the interactive watch mode.<br />
See the section about [running tests](https://facebook.github.io/create-react-app/docs/running-tests) for more information.

### `npm run build`

Builds the app for production to the `build` folder.<br />
It correctly bundles React in production mode and optimizes the build for the best performance.

The build is minified and the filenames include the hashes.<br />
Your app is ready to be deployed!

See the section about [deployment](https://facebook.github.io/create-react-app/docs/deployment) for more information.


### Icons used
#### Material-UI icons are used

Installation
```
//For Grid, Typography,Button, Paper,etc... Components
npm install @material-ui/core

//For LocationOn Button
npm i @material-ui/icons


```

### Deployment

Deloping Website in Firebase.

##### steps

1. Install Firebase Tools
```
    npm install -g firebase-tools
```
2. Initialize the Firebase
```
    firebase login
    firebase init
```
In initialization we need to select Hosting by press space and then select build version of our website.
select the options like below
#### steps
```
? What do you want to use as your public directory? build   [select build]
? Configure as a single-page app (rewrite all urls to /index.html)? Yes  [select Yes]
? File build/index.html already exists. Overwrite? No [select No]
```
 
4.Deploy
```
    firebase deploy
```
Our Website Output(https://ftl-task.web.app/)

## output<br />
[userList Output](https://gitlab.com/sai721/ftlassignment/-/blob/master/output/all_users.PNG)<br />
[user Without Activity on Particular Day](https://gitlab.com/sai721/ftlassignment/-/blob/master/output/single_user_no_activity.PNG)<br />
[user With Activity on Selected Day](https://gitlab.com/sai721/ftlassignment/-/blob/master/output/single_user_with_activity.PNG)<br />



### `npm run eject`

**Note: this is a one-way operation. Once you `eject`, you can’t go back!**

If you aren’t satisfied with the build tool and configuration choices, you can `eject` at any time. This command will remove the single build dependency from your project.

Instead, it will copy all the configuration files and the transitive dependencies (webpack, Babel, ESLint, etc) right into your project so you have full control over them. All of the commands except `eject` will still work, but they will point to the copied scripts so you can tweak them. At this point you’re on your own.

You don’t have to ever use `eject`. The curated feature set is suitable for small and middle deployments, and you shouldn’t feel obligated to use this feature. However we understand that this tool wouldn’t be useful if you couldn’t customize it when you are ready for it.






## Learn More

You can learn more in the [Create React App documentation](https://facebook.github.io/create-react-app/docs/getting-started).

To learn React, check out the [React documentation](https://reactjs.org/).

### Code Splitting

This section has moved here: https://facebook.github.io/create-react-app/docs/code-splitting

### Analyzing the Bundle Size

This section has moved here: https://facebook.github.io/create-react-app/docs/analyzing-the-bundle-size

### Making a Progressive Web App

This section has moved here: https://facebook.github.io/create-react-app/docs/making-a-progressive-web-app

### Advanced Configuration

This section has moved here: https://facebook.github.io/create-react-app/docs/advanced-configuration


### `npm run build` fails to minify

This section has moved here: https://facebook.github.io/create-react-app/docs/troubleshooting#npm-run-build-fails-to-minify
