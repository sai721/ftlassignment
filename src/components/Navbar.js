import React, { Component,Fragment } from 'react'
import withStyles from '@material-ui/core/styles/withStyles'

//mui
import  AppBar from "@material-ui/core/AppBar";
import  Toolbar from "@material-ui/core/Toolbar";
import PropTypes  from 'prop-types';

const styles = {
    head: {
        textAlign: 'center',

    }
}

class Navbar extends Component {
    render() {
        const {classes }= this.props;
        return (
            <div>
                <AppBar>
                    <Toolbar className="nav-container">
                        <Fragment>
                            <h2 className={classes.head}>Activity Tracking</h2>
                        </Fragment>
                    </Toolbar>
                </AppBar>
            </div>
        )
    }
}


Navbar.propTypes = {
        classes: PropTypes.object.isRequired

} 
export default withStyles(styles)(Navbar)
  