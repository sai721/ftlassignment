import React, { Component } from 'react'

import withStyles from '@material-ui/core/styles/withStyles'
import {Link} from 'react-router-dom'
import NoImg from '../images/no-img.png'

import Card from '@material-ui/core/Card';
import PropTypes  from 'prop-types';

import CardMedia from '@material-ui/core/CardMedia';
import CardContent from '@material-ui/core/CardContent';

import { Typography } from '@material-ui/core';


//icons
import LocationOn from '@material-ui/icons/LocationOn';

const styles = {
    card: {
        display: 'flex',
        marginBottom: 20,
        marginTop: 10,
    },
    cardContent: {
        width: '100%',
        flexDirection: 'column',
        padding: 50
    },
    cover: {
        minWidth: 100,
        objectFit: 'cover',

    },
    id: {
        marginTop: 10,
    },
    name: {
        marginTop: 10,
    },
    location: {
        marginTop: 10,
    }


}


class User extends Component {
    
    render() {
        const {classes, member: {id, real_name, tz}, loc} = this.props;
        
        return (
                <Card className={classes.card}>
                <CardMedia className={classes.cover} image={NoImg} />

                <CardContent className={classes.content}>
                        {loc ? (
                        <Typography 
                        variant="h5" 
                        component={Link} to={`/users/${id}`} 
                        color="primary" className={classes.id}> {id}</Typography>) 
                        : (
                        <Typography 
                            variant="h5"
                            color="primary" className={classes.id}> {id}</Typography>)}
                        

                    <Typography variant="body2"  className={classes.name}>
                        <span>{"Name: "}{real_name}</span>
                    </Typography>

                    <Typography variant="body2"  className={classes.location}>
                        <LocationOn color="primary" /><span>{tz}</span>
                    </Typography>

                </CardContent>
            </Card>
            
        )
    }
}

User.propTypes = {
    member: PropTypes.object.isRequired,
    classes: PropTypes.object.isRequired

}

export default withStyles(styles)(User)
