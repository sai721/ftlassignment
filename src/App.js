
import React, { Component } from 'react'
import {BrowserRouter as Router, Route, Switch} from 'react-router-dom'

import Navbar from './components/Navbar.js'
import home from './pages/home'
import track from './pages/track'

//theme
import { ThemeProvider as MuiThemeProvider } from '@material-ui/core/styles'
import createMuiTheme from "@material-ui/core/styles/createMuiTheme";

const theme=createMuiTheme({
  palette: {
    primary: {
      light: '#33c9dc',
      main: '#00bcd4',
      dark: '#008394',
      contrastText: '#fff'
    },
    secondary:{
      light: '#ff6333',
      main: '#ff3d00',
      dark: '#b22a00',
      contrastText: '#fff'
    }

  },
  typography:{
    useNextVariations: true,
  },

});


class App extends Component {
  render(){
    return (
      <MuiThemeProvider theme={theme}>
      
      <Router >
          <Navbar/>
          <div className="container">
            <Switch>
            <Route exact  path="/" 
              component={home} 
              />
              
              <Route exact  path="/users/:id" 
              component={track} 
              />
               
            </Switch>
          </div>
        </Router>
        
      
      </MuiThemeProvider>
    );
  }
  
}



export default App;
