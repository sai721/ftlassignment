import React, { Component} from 'react'
import withStyles from '@material-ui/core/styles/withStyles'
import { Grid } from '@material-ui/core'
import Calendar from 'react-calendar';
import {Link} from 'react-router-dom'


import data from '../data/data.json'
import User from '../components/User'
import { Typography } from '@material-ui/core';

import 'react-calendar/dist/Calendar.css';

import Paper from '@material-ui/core/Paper'
import { Tooltip } from '@material-ui/core'
import IconButton from '@material-ui/core/IconButton';
import KeyboardReturnIcon from '@material-ui/icons/KeyboardReturn';


const styles = {
    paper: {
        padding: 20,
    },
    para: {
        textAlign:'center'
    },
    users: {
        padding: 20,
        marginTop: 50,
    },
    
}
class track extends Component {
    state = {
        member:{},
        date: new Date(),
        same: false,
        startHr: "",
        endHr: "",
    }
    componentDidMount() {
        const id=this.props.match.params.id;
        console.log(id);
        let member=data.members.filter(member => member.id=== id )[0]

        this.setState({member:member})
        
      } 

      onChange = date => {
        console.log(date)
        this.setState({ date:date })
        let presentDate=new Date(date);
        this.match(presentDate);    
     }

     match = presentDate => {
         let time=this.state.member.activity_periods;
         let flag=false;
         let startHr="";
         let endHr="";
         for(var i=0;i<time.length;i++)
         {
             let dates=time[i];
             let startTime=dates.start_time.split(" ")
             let startDate=new Date(startTime[0]+" "+startTime[1]+" "+startTime[2])
             startHr=startTime[4]

             let endTime=dates.end_time.split(" ")
             endHr=endTime[3]
             
             if(startDate.getTime() === presentDate.getTime())
             {
                 flag=true;
                 break
             }       

             console.log(this.state.same);
             
         }
         
         this.setState({
            startHr: startHr,
            endHr: endHr,
            same: flag,
        })     
     }

     disable = ({activeStartDate, date, view }) => date.getTime() > new Date();
     

    render() {
        const {classes }= this.props;
        let activeMarkup = (this.state.same ? (
            <Typography variant="body2"  className={classes.name}>
            <span>Activity Found, starts from {this.state.startHr} Ended at {this.state.endHr}</span>         
        </Typography>)
         : (
        <Typography variant="body2"  className={classes.name}>
            <span>No activity found  on {this.state.date.toLocaleDateString()}</span>
        </Typography>));
        return (
            <div className={classes.users}>
                <p className={classes.para}>Select date from calender to see previous activity</p>
            
                <Grid container >
                    <Grid item sm={8} xs={12}>
                    <div >
                        <User key={this.state.member.id} member={this.state.member} loc={false}/>
                        
                        <Paper >
                            <div className={classes.paper}>
                                {activeMarkup}
                                
                            </div>


                                <Tooltip title="Go back"  placement="top">
                                    <IconButton className={classes.button} component={Link} to="/">
                                        <KeyboardReturnIcon  color="primary"/>
                                    </IconButton>
                                </Tooltip>
                            

                            
                        </Paper>

                    </div>      
                    </Grid>

                    <Grid item sm={4} xs={12}>
                     <Calendar
                        onChange={this.onChange}
                        value={this.state.date}
                        tileDisabled={this.disable}
                        />        
                    </Grid>
                </Grid>
                </div>
        );
    }
}



export default withStyles(styles)(track)
