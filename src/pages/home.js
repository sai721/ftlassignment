
import React, { Component } from 'react'
import withStyles from '@material-ui/core/styles/withStyles'

import data from '../data/data.json'
import User from '../components/User'
import PropTypes  from 'prop-types';

const styles= {
    users: {
        padding: 20,
        marginTop: 50,
    }
}
export class home extends Component {
  state = {
    data: ""
  }
  componentDidMount() {
    this.setState({data})   
  }
  render() {
    const {classes} = this.props;
    let makingUsers= (
      data.members.map(member => <User key={member.id} member={member} loc={true}/>)
    )
    return (
      <div className={classes.users}>
          {makingUsers}
      </div>
    )
  }
}

home.propTypes = {
        classes: PropTypes.object.isRequired

}

export default withStyles(styles)(home)

